Python Parallel Hierarchy
=========================

A utility for creating a parallel class hierarchy based on a primary set of classes.

How does it work?
-----------------
This package gives you an easy to use factory.  This factory takes as input a class from an original model, and outputs
a new class in a new parallel inheritance structure.

More information at https://gitlab.com/joel.larose/python-parallel-hierarchy/-/blob/master/README.rst

How to install
--------------
.. code::

    pip install https://gitlab.com/joel.larose/python-parallel-hierarchy.git

or

.. code::

    pip install python-parallel-hierarchy

